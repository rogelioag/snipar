﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    public int vueltas;

    public int target;

    public Transform[] objetivoPiso1;
    public Transform[] objetivoPiso2;

    //Variables de Vida
    public Scrollbar liveBar;
    public float live;

    //Variables de puntos
    public Text points;
    public int score;

    //Variables de balas
    public Image[] bulletsImg;
    public int bullets;

    public bool missShot;


    // Use this for initialization
    void Start ()
    {
        //Instantiate()
        missShot = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        liveBar.size = live;
        points.text = score.ToString();

        SetBulletsGUI();  
	}

    void SetBulletsGUI()
    {
        switch (bullets)
        {
            case 0:
                for (int i = 0; i < bulletsImg.Length; i++)
                {
                    bulletsImg[i].enabled = false;
                }
                for (int i = 0; i < bulletsImg.Length - 5; i++)
                {
                    bulletsImg[i].enabled = true;
                }
                break;
            case 1:
                for (int i = 1; i < bulletsImg.Length; i++)
                {
                    bulletsImg[i].enabled = false;
                }
                for (int i = 0; i < bulletsImg.Length-4; i++)
                {
                    bulletsImg[i].enabled = true;
                }
                break;
            case 2:
                for (int i = 2; i < bulletsImg.Length; i++)
                {
                    bulletsImg[i].enabled = false;
                }
                for (int i = 0; i < bulletsImg.Length - 3; i++)
                {
                    bulletsImg[i].enabled = true;
                }
                break;
            case 3:
                for (int i = 3; i < bulletsImg.Length; i++)
                {
                    bulletsImg[i].enabled = false;
                }
                for (int i = 0; i < bulletsImg.Length - 2; i++)
                {
                    bulletsImg[i].enabled = true;
                }
                break;
            case 4:
                for (int i = 4; i < bulletsImg.Length; i++)
                {
                    bulletsImg[i].enabled = false;
                }
                for (int i = 0; i < bulletsImg.Length - 1; i++)
                {
                    bulletsImg[i].enabled = true;
                }
                break;
            case 5:
                for (int i = 5; i < bulletsImg.Length; i++)
                {
                    bulletsImg[i].enabled = false;
                }
                for (int i = 0; i < bulletsImg.Length; i++)
                {
                    bulletsImg[i].enabled = true;
                }
                break;
        }
    }
}
