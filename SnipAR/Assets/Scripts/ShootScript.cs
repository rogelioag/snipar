﻿using UnityEngine;
using System.Collections;

public class ShootScript : MonoBehaviour
{
    public GameObject lineShoot;
    public GameObject blood;
    public GameObject bulletHole;

    private Ray ray;
    private RaycastHit rayHit;

    private TargetBehaivour objectHit;

    GameControl gameControl;

    // Use this for initialization
    void Start ()
    {
        gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void Shoot()
    {
        gameControl.bullets -= 1;

        lineShoot.SetActive(true);
        StartCoroutine(ResetLineShoot());

        ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2,Screen.height / 2));
        if(Physics.Raycast(ray, out rayHit))
        {
            print(rayHit.collider.gameObject.name);

            if (rayHit.collider.gameObject.tag == "head")
            {
                print("Is Dead");
                GameObject sangre = Instantiate(blood, rayHit.collider.gameObject.transform.position, Quaternion.identity) as GameObject;
                Destroy(sangre, 1f);

                //Funcion del target
                rayHit.collider.gameObject.GetComponentInParent<TargetBehaivour>().Dies();
            }
            else if (rayHit.collider.gameObject.tag == "body")
            {
                print("Is Hurt");
                GameObject sangre = Instantiate(blood, rayHit.collider.gameObject.transform.position, Quaternion.identity) as GameObject;
                Destroy(sangre, 1f);

                //Funcion del target
                rayHit.collider.gameObject.GetComponentInParent<TargetBehaivour>().Hurt();
            }
            else if (rayHit.collider.gameObject.tag == "leg")
            {
                print("Is Slowed");
                GameObject sangre = Instantiate(blood, rayHit.collider.gameObject.transform.position, Quaternion.identity) as GameObject;
                Destroy(sangre, 1f);

                //Funcion del target
                rayHit.collider.gameObject.GetComponentInParent<TargetBehaivour>().LegHurt();
            }

            else
            {
                GameObject chispas = Instantiate(bulletHole, rayHit.point, transform.rotation) as GameObject;
                Destroy(chispas, 5.5f);

                gameControl.missShot = true;
            }
        }
    }

    public IEnumerator ResetLineShoot()
    {
        yield return new WaitForSeconds(0.05f);
        lineShoot.SetActive(false);
        gameControl.missShot = false;
    }
}
