﻿using UnityEngine;
using System.Collections;

public class TargetBehaivour : MonoBehaviour
{
    GameControl gamecontrol;

    public Transform[] objetivoPiso1;
    public Transform[] objetivoPiso2;
    public Transform[] objetivoPiso3;
    public Transform[] objetivoPiso4;
    public Transform[] objetivoPiso5;
    public Transform[] objetivoPiso6;
    public Transform[] objetivoEscape;

    Vector3 destination;
    public NavMeshAgent navegador;

    int cont;
    public int vueltas;
    public int isInFloor;

    public GameObject piso1;
    public GameObject piso2;
    public GameObject piso3;
    public GameObject piso4;
    public GameObject piso5;
    public GameObject piso6;

    // Use this for initialization
    void Start()
    {
        gamecontrol = GameObject.Find("GameControl").GetComponent<GameControl>();

        navegador = GetComponent<NavMeshAgent>();
       // cont = 0;
      //  vueltas = 0;
       // isInFloor = 1;
    }

    // Update is called once per frame
    void Update()
    {
        StandarMovement();
        if(gamecontrol.missShot)
        {
            ChangeFloor(0);
            gamecontrol.missShot = false;            
        }
    }

    void FixedUpdate()
    {
       
    }

    public void Dies()
    {
        gamecontrol.score += 50;
        Destroy(gameObject);
    }

    public void LegHurt()
    {
        gamecontrol.score += 10;
        navegador.speed -= 0.5f;
    }

    public void Hurt()
    {
        gamecontrol.score += 5;
        navegador.speed -= 0.2f;
    }

    public void StandarMovement()
    {

        switch (isInFloor)
        {
            case 1:
                SetRoute(objetivoPiso1);
                break;
            case 2:
                SetRoute(objetivoPiso2);
                break;
            case 3: 
                SetRoute(objetivoPiso3);
                break;
            case 4:
                SetRoute(objetivoPiso4);
                break;
            case 5:
                SetRoute(objetivoPiso5);
                break;
            case 6:
                SetRoute(objetivoPiso6);
                break;
            case 0:
                SetRoute(objetivoEscape);
                break;
        }        

        if (vueltas == 3)
        {
            int randomFloor = Random.Range(1, 7);

            while(randomFloor == isInFloor)
            {
                randomFloor = Random.Range(1, 7);               
            }

            isInFloor = randomFloor;
            ChangeFloor(isInFloor);
            vueltas = 0;
        }

       navegador.SetDestination(destination);
    }

    public void ChangeFloor(int floor)
    {
        switch(floor)
        {
            case 1:
                GameObject newTarget = Instantiate(gameObject, new Vector3(transform.position.x, piso1.transform.position.y + 1.45f, transform.position.z), Quaternion.identity) as GameObject;
                newTarget.transform.SetParent(GameObject.Find("ImageTarget").transform);
                newTarget.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
                newTarget.GetComponent<TargetBehaivour>().isInFloor = floor;
                newTarget.GetComponent<TargetBehaivour>().cont = 0;
                newTarget.GetComponent<TargetBehaivour>().vueltas = 0;
                break;
            case 2:
                GameObject newTarget2 = Instantiate(gameObject, new Vector3(transform.position.x, piso2.transform.position.y + 1.4f, transform.position.z), Quaternion.identity) as GameObject;
                newTarget2.transform.SetParent(GameObject.Find("ImageTarget").transform);
                newTarget2.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
                newTarget2.GetComponent<TargetBehaivour>().isInFloor = floor;
                newTarget2.GetComponent<TargetBehaivour>().cont = 0;
                newTarget2.GetComponent<TargetBehaivour>().vueltas = 0;
                break;
            case 3:
                GameObject newTarget3 = Instantiate(gameObject, new Vector3(transform.position.x, piso3.transform.position.y + 1.45f, transform.position.z), Quaternion.identity) as GameObject;
                newTarget3.transform.SetParent(GameObject.Find("ImageTarget").transform);
                newTarget3.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
                newTarget3.GetComponent<TargetBehaivour>().isInFloor = floor;
                newTarget3.GetComponent<TargetBehaivour>().cont = 0;
                newTarget3.GetComponent<TargetBehaivour>().vueltas = 0;
                break;
            case 4:
                GameObject newTarget4 = Instantiate(gameObject, new Vector3(transform.position.x, piso4.transform.position.y + 1.45f, transform.position.z), Quaternion.identity) as GameObject;
                newTarget4.transform.SetParent(GameObject.Find("ImageTarget").transform);
                newTarget4.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
                newTarget4.GetComponent<TargetBehaivour>().isInFloor = floor;
                newTarget4.GetComponent<TargetBehaivour>().cont = 0;
                newTarget4.GetComponent<TargetBehaivour>().vueltas = 0;
                break;
            case 5:
                GameObject newTarget5 = Instantiate(gameObject, new Vector3(transform.position.x, piso5.transform.position.y + 1.45f, transform.position.z), Quaternion.identity) as GameObject;
                newTarget5.transform.SetParent(GameObject.Find("ImageTarget").transform);
                newTarget5.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
                newTarget5.GetComponent<TargetBehaivour>().isInFloor = floor;
                newTarget5.GetComponent<TargetBehaivour>().cont = 0;
                newTarget5.GetComponent<TargetBehaivour>().vueltas = 0;
                break;
            case 6:
                GameObject newTarget6 = Instantiate(gameObject, new Vector3(transform.position.x, piso6.transform.position.y + 1.45f, transform.position.z), Quaternion.identity) as GameObject;
                newTarget6.transform.SetParent(GameObject.Find("ImageTarget").transform);
                newTarget6.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
                newTarget6.GetComponent<TargetBehaivour>().isInFloor = floor;
                newTarget6.GetComponent<TargetBehaivour>().cont = 0;
                newTarget6.GetComponent<TargetBehaivour>().vueltas = 0;
                break;
            case 0:
                GameObject newTarget0 = Instantiate(gameObject, new Vector3(transform.position.x, piso1.transform.position.y + 1.45f, transform.position.z), Quaternion.identity) as GameObject;
                newTarget0.transform.SetParent(GameObject.Find("ImageTarget").transform);
                newTarget0.transform.localScale = new Vector3(0.07f, 0.07f, 0.07f);
                newTarget0.GetComponent<TargetBehaivour>().isInFloor = floor;
                newTarget0.GetComponent<TargetBehaivour>().cont = 0;
                newTarget0.GetComponent<TargetBehaivour>().vueltas = 0;
                break;
        }
        Destroy(gameObject, 0.3f);

        //isInFloor = 2;
        //cont = 0;        
    }

    public void SetRoute(Transform[] rutaDePiso)
    {
        destination = rutaDePiso[cont].position;

        if (Vector3.Distance(destination, transform.position) <= 0.5f)
        {
            print("En el piso " + isInFloor);
            cont++;

            //Piso 1
            if (cont >= rutaDePiso.Length)
            {
                //Validacion de escape
                if (isInFloor == 0)
                {
                    navegador.Stop();
                }

                cont = 0;
                vueltas++;
            }            
        }
    }
}
